﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace keyLogs
{
    class ClassData
    {
        #region ClassData
        public static string GetConnectionString()
        {
            // To avoid storing the connection string in your code,
            // you can retrive it from a configuration file.
            return "Data Source=" + Program.server
                     + ";Initial Catalog=" + Program.dataname
                     + ";User Id=" + Program.user + ";Password=" + Program.pw
                     + ";Integrated Security=no;MultipleActiveResultSets=True;Packet Size=4096;Pooling=false;";
        }
        //Thực thi câu lệnh
        public static void ExecuteSQL(string sql)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(GetConnectionString()))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = sql;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new Exception("Có lỗi trong quá trình thực thi câu lệnh\n" + sqlException.Message);
            }
        }
        //trả về string
        public static string GetStringSQL(string sql)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(GetConnectionString()))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = sql;
                    con.Open();
                    var res = sqlCommand.ExecuteScalar();
                    if (res == null)
                        return "";
                    else
                        return res.ToString();
                }
            }
            catch (SqlException sqlException)
            {
                throw new Exception("Có lỗi trong quá trình lấy string\n" + sqlException.Message);
            }
        }
        //trả về int
        public static int GetIntSQL(string sql)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(GetConnectionString()))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = sql;
                    con.Open();
                    var res = sqlCommand.ExecuteScalar();
                    if (res == null)
                        return -1;
                    else
                        try
                        {
                            return Convert.ToInt32(res.ToString());
                        }
                        catch
                        {
                            return 0;
                        }
                }
            }
            catch (SqlException sqlException)
            {
                throw new Exception("Có lỗi trong quá trình lấy int\n" + sqlException.Message);
            }
        }
        //trả về datatable
        public static DataTable GetDatatableSQL(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(GetConnectionString()))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = sql;
                    SqlDataAdapter daUser = new SqlDataAdapter();
                    daUser.SelectCommand = sqlCommand;
                    con.Open();
                    daUser.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new Exception("Có lỗi trong quá trình lấy datatable\n" + sqlException.Message);
            }
            return dt;
        }
        public static byte[] GetByteSQL(string sql)
        {
            byte[] image = null;
            try
            {
                using (SqlConnection con = new SqlConnection(GetConnectionString()))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = sql;
                    con.Open();
                    image = (byte[])sqlCommand.ExecuteScalar();
                }
            }
            catch (SqlException sqlException)
            {
                throw new Exception("Có lỗi trong quá trình lấy image\n" + sqlException.Message);
            }
            return image;
        }
        #endregion

    }
}
