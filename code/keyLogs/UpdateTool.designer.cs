﻿namespace keyLogs
{
    partial class UpdateTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btChonExe = new System.Windows.Forms.Button();
            this.lbTenFiledll = new System.Windows.Forms.Label();
            this.btUpExe = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grdFile = new System.Windows.Forms.DataGridView();
            this.cbTenFile = new System.Windows.Forms.ComboBox();
            this.btDownLoadEXE = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabEXE = new System.Windows.Forms.TabPage();
            this.lbTenFilexe = new System.Windows.Forms.Label();
            this.tabDLL = new System.Windows.Forms.TabPage();
            this.btUpDll = new System.Windows.Forms.Button();
            this.btChonDLL = new System.Windows.Forms.Button();
            this.btdel = new System.Windows.Forms.Button();
            this.btDownDLL = new System.Windows.Forms.Button();
            this.cbTenFileDll = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grdFileDll = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdFile)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabEXE.SuspendLayout();
            this.tabDLL.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdFileDll)).BeginInit();
            this.SuspendLayout();
            // 
            // btChonExe
            // 
            this.btChonExe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btChonExe.Location = new System.Drawing.Point(6, 6);
            this.btChonExe.Name = "btChonExe";
            this.btChonExe.Size = new System.Drawing.Size(67, 33);
            this.btChonExe.TabIndex = 0;
            this.btChonExe.Text = "Chọn:";
            this.btChonExe.UseVisualStyleBackColor = true;
            this.btChonExe.Click += new System.EventHandler(this.btChonExe_Click);
            // 
            // lbTenFiledll
            // 
            this.lbTenFiledll.AutoSize = true;
            this.lbTenFiledll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenFiledll.Location = new System.Drawing.Point(104, 12);
            this.lbTenFiledll.Name = "lbTenFiledll";
            this.lbTenFiledll.Size = new System.Drawing.Size(19, 20);
            this.lbTenFiledll.TabIndex = 1;
            this.lbTenFiledll.Text = "_";
            // 
            // btUpExe
            // 
            this.btUpExe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUpExe.Location = new System.Drawing.Point(406, 11);
            this.btUpExe.Name = "btUpExe";
            this.btUpExe.Size = new System.Drawing.Size(75, 28);
            this.btUpExe.TabIndex = 2;
            this.btUpExe.Text = "UP";
            this.btUpExe.UseVisualStyleBackColor = true;
            this.btUpExe.Click += new System.EventHandler(this.btUpExe_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.grdFile);
            this.panel1.Location = new System.Drawing.Point(6, 84);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(480, 263);
            this.panel1.TabIndex = 3;
            // 
            // grdFile
            // 
            this.grdFile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdFile.Location = new System.Drawing.Point(0, 0);
            this.grdFile.Name = "grdFile";
            this.grdFile.Size = new System.Drawing.Size(480, 263);
            this.grdFile.TabIndex = 0;
            // 
            // cbTenFile
            // 
            this.cbTenFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTenFile.FormattingEnabled = true;
            this.cbTenFile.Location = new System.Drawing.Point(6, 45);
            this.cbTenFile.Name = "cbTenFile";
            this.cbTenFile.Size = new System.Drawing.Size(394, 28);
            this.cbTenFile.TabIndex = 4;
            // 
            // btDownLoadEXE
            // 
            this.btDownLoadEXE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDownLoadEXE.Location = new System.Drawing.Point(406, 45);
            this.btDownLoadEXE.Name = "btDownLoadEXE";
            this.btDownLoadEXE.Size = new System.Drawing.Size(75, 28);
            this.btDownLoadEXE.TabIndex = 5;
            this.btDownLoadEXE.Text = "Down";
            this.btDownLoadEXE.UseVisualStyleBackColor = true;
            this.btDownLoadEXE.Click += new System.EventHandler(this.btDownLoad_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabEXE);
            this.tabControl1.Controls.Add(this.tabDLL);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(497, 379);
            this.tabControl1.TabIndex = 7;
            // 
            // tabEXE
            // 
            this.tabEXE.Controls.Add(this.btUpExe);
            this.tabEXE.Controls.Add(this.lbTenFilexe);
            this.tabEXE.Controls.Add(this.btChonExe);
            this.tabEXE.Controls.Add(this.cbTenFile);
            this.tabEXE.Controls.Add(this.panel1);
            this.tabEXE.Controls.Add(this.btDownLoadEXE);
            this.tabEXE.Location = new System.Drawing.Point(4, 22);
            this.tabEXE.Name = "tabEXE";
            this.tabEXE.Padding = new System.Windows.Forms.Padding(3);
            this.tabEXE.Size = new System.Drawing.Size(489, 353);
            this.tabEXE.TabIndex = 0;
            this.tabEXE.Text = "FileEXE";
            this.tabEXE.UseVisualStyleBackColor = true;
            // 
            // lbTenFilexe
            // 
            this.lbTenFilexe.AutoSize = true;
            this.lbTenFilexe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenFilexe.Location = new System.Drawing.Point(79, 12);
            this.lbTenFilexe.Name = "lbTenFilexe";
            this.lbTenFilexe.Size = new System.Drawing.Size(19, 20);
            this.lbTenFilexe.TabIndex = 6;
            this.lbTenFilexe.Text = "_";
            // 
            // tabDLL
            // 
            this.tabDLL.Controls.Add(this.btUpDll);
            this.tabDLL.Controls.Add(this.btChonDLL);
            this.tabDLL.Controls.Add(this.btdel);
            this.tabDLL.Controls.Add(this.lbTenFiledll);
            this.tabDLL.Controls.Add(this.btDownDLL);
            this.tabDLL.Controls.Add(this.cbTenFileDll);
            this.tabDLL.Controls.Add(this.panel2);
            this.tabDLL.Controls.Add(this.label1);
            this.tabDLL.Location = new System.Drawing.Point(4, 22);
            this.tabDLL.Name = "tabDLL";
            this.tabDLL.Padding = new System.Windows.Forms.Padding(3);
            this.tabDLL.Size = new System.Drawing.Size(489, 353);
            this.tabDLL.TabIndex = 1;
            this.tabDLL.Text = "FileDLL";
            this.tabDLL.UseVisualStyleBackColor = true;
            // 
            // btUpDll
            // 
            this.btUpDll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUpDll.Location = new System.Drawing.Point(391, 8);
            this.btUpDll.Name = "btUpDll";
            this.btUpDll.Size = new System.Drawing.Size(75, 28);
            this.btUpDll.TabIndex = 9;
            this.btUpDll.Text = "UP";
            this.btUpDll.UseVisualStyleBackColor = true;
            this.btUpDll.Click += new System.EventHandler(this.btUpDll_Click);
            // 
            // btChonDLL
            // 
            this.btChonDLL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btChonDLL.Location = new System.Drawing.Point(5, 6);
            this.btChonDLL.Name = "btChonDLL";
            this.btChonDLL.Size = new System.Drawing.Size(67, 33);
            this.btChonDLL.TabIndex = 14;
            this.btChonDLL.Text = "Chọn:";
            this.btChonDLL.UseVisualStyleBackColor = true;
            this.btChonDLL.Click += new System.EventHandler(this.btChonDLL_Click);
            // 
            // btdel
            // 
            this.btdel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btdel.Location = new System.Drawing.Point(429, 44);
            this.btdel.Name = "btdel";
            this.btdel.Size = new System.Drawing.Size(53, 28);
            this.btdel.TabIndex = 13;
            this.btdel.Text = "Del";
            this.btdel.UseVisualStyleBackColor = true;
            this.btdel.Click += new System.EventHandler(this.btdel_Click);
            // 
            // btDownDLL
            // 
            this.btDownDLL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDownDLL.Location = new System.Drawing.Point(370, 44);
            this.btDownDLL.Name = "btDownDLL";
            this.btDownDLL.Size = new System.Drawing.Size(53, 28);
            this.btDownDLL.TabIndex = 12;
            this.btDownDLL.Text = "DownLoad";
            this.btDownDLL.UseVisualStyleBackColor = true;
            this.btDownDLL.Click += new System.EventHandler(this.btDownDLL_Click);
            // 
            // cbTenFileDll
            // 
            this.cbTenFileDll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTenFileDll.FormattingEnabled = true;
            this.cbTenFileDll.Location = new System.Drawing.Point(6, 45);
            this.cbTenFileDll.Name = "cbTenFileDll";
            this.cbTenFileDll.Size = new System.Drawing.Size(352, 28);
            this.cbTenFileDll.TabIndex = 11;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.grdFileDll);
            this.panel2.Location = new System.Drawing.Point(6, 79);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(477, 266);
            this.panel2.TabIndex = 10;
            // 
            // grdFileDll
            // 
            this.grdFileDll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdFileDll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdFileDll.Location = new System.Drawing.Point(0, 0);
            this.grdFileDll.Name = "grdFileDll";
            this.grdFileDll.Size = new System.Drawing.Size(477, 266);
            this.grdFileDll.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(162, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 20);
            this.label1.TabIndex = 8;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // UpdateTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 379);
            this.Controls.Add(this.tabControl1);
            this.Name = "UpdateTool";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UpdateTool_FormClosing);
            this.Load += new System.EventHandler(this.UpdateTool_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdFile)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabEXE.ResumeLayout(false);
            this.tabEXE.PerformLayout();
            this.tabDLL.ResumeLayout(false);
            this.tabDLL.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdFileDll)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btChonExe;
        private System.Windows.Forms.Label lbTenFiledll;
        private System.Windows.Forms.Button btUpExe;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbTenFile;
        private System.Windows.Forms.Button btDownLoadEXE;
        private System.Windows.Forms.DataGridView grdFile;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabEXE;
        private System.Windows.Forms.TabPage tabDLL;
        private System.Windows.Forms.Button btdel;
        private System.Windows.Forms.Button btDownDLL;
        private System.Windows.Forms.ComboBox cbTenFileDll;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView grdFileDll;
        private System.Windows.Forms.Button btUpDll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbTenFilexe;
        private System.Windows.Forms.Button btChonDLL;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }

    partial class inPutPass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPass = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtPass
            // 
            this.txtPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPass.Location = new System.Drawing.Point(13, 13);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(320, 22);
            this.txtPass.TabIndex = 0;
            // 
            // inPutPass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 53);
            this.Controls.Add(this.txtPass);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "inPutPass";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhập Mật Khẩu";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.inPutPass_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inPutPass_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPass;
    }
}